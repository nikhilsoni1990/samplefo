var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var express = require('express');
module.exports.configureApp = function(app) {
    //app.set('port', process.env.PORT || 3500);
    // view engine setup
    app.set('views', path.join(__dirname, '../angular'));
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, '../angular')));
    app.use(methodOverride('X-HTTP-Method-Override'));
    app.set('superSecret', "Nikhil");
};

