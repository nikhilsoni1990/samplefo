data = {
				items: [
					{ name: 'Apples', price: 2400 }, { name: 'Bananas', price: 1000 }, { name: 'Pears', price:1000 }
				],
				rows: [{
				    tenantName: 'Nikhil Soni',
					deviceId: 'JBL-9881771',
					description: 'KUKA 12V SML',
					location: 'LISBON, SPAIN',
					year: '1990',
					utilization: '90%',
					vendor1: '09/12/2016',
					status: '1 2 3',
					respOp: 'Mike T',
					savings: '20%',
					qty: '3122',
					cost: '$99,999',
					value: '$29,999,999.00'
				}, {
				    tenantName: 'TEST2',
					deviceId: 'JBL-9881772',
					description: 'FESTO LRG 123-1121',
					location: 'SHANGHAI, CHINA',
					year: '1991',
					utilization: '90%',
					vendor1: '09/16/2016',
					status: '1 2 3',
					respOp: 'Jamie Z',
					savings: '20%',
					qty: '3122',
					cost: '$99,999',
					value: '$29,999,999.00'
				}, {
				    tenantName: 'TEST3',
					deviceId: 'JBL-9881773',
					description: 'FESTO LRG 123-1121',
					location: 'LISBON, SPAIN',
					year: '1989',
					utilization: '50%',
					vendor1: '09/20/2016',
					status: '1 2 3',
					respOp: 'Frank W',
					savings: '50%',
					qty: '3122',
					cost: '$99,999',
					value: '$29,999,999.00'
				}]
			}
