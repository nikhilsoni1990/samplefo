var dataTable;
function onEquipmentSearchKeyUp() {
    refreshTable();
}
function refreshTable() {

	dataTable.rows().remove().draw( false );

	var searchFilter = document.getElementById('search-organization-people2').value.replace(/\s$/, '').replace(/^\s/, '').toLowerCase();
	
	var deviceFilter = $('#neural-select-device').val();

	for (var i = data.rows.length; i--;) {
		var row = data.rows[i];

		if (!searchFilter || row.deviceId.toLowerCase().indexOf(searchFilter) != -1 || row.description.toLowerCase().indexOf(searchFilter) != -1 || row.location.toLowerCase().indexOf(searchFilter) != -1 || row.respOp.toLowerCase().indexOf(searchFilter) != -1) {
		
			if (!deviceFilter || deviceFilter.indexOf(row.deviceId) != -1) {
			    dataTable.row.add([
                    row.tenantName || '',
		            row.deviceId || '',
		            row.description || '',
		            row.location || '',
					row.year || '',
					row.utilization || '',
					row.vendor1 || '',
					'<img src="assets/img/data_table_BlueStatus_16.fw.png"><img src="assets/img/data_table_ClearStatus_16.fw.png"><img src="assets/img/data_table_RedStatus_16.fw.png">',
					row.respOp || '',
					row.qty,
					row.cost,
					row.value
		        ] ).draw( false );
			}
		}
	}
}
