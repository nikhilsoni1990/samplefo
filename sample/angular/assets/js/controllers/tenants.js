'use strict';

/* Controllers */

angular.module('app')
    .controller('TenantsCTRL', function ($scope, $state, $stateParams, $http) {
        $scope.init = function () {

            $('#neural-filter-avl,#neural-filter-conflict,#neural-filter-aml,#neural-filter-rare-earths').select2();

            function updateFilters() {
                console.log('TODO: Update filter selects');
            }


            var table = initDetailedViewTable();

            dataTable = table.DataTable({
                "aoColumnDefs": [
                    { "class": "numeric", "aTargets": [8, 9, 10] },
                    { "class": "status", "aTargets": [6] }
                ],
                "bSort": true
            });

            refreshTable();
        }

        $scope.addTenant = function () {
            $http({
                "url": 'http://ec2-52-53-208-151.us-west-1.compute.amazonaws.com:8000/config/tenant/',
                "method": "POST",
                "headers": {
                    "Content-Type": "application/json",
                    "authorization": 'Bearer ' + localStorage.getItem('jwt'),
                },
                "cache": true,
                "data": {payload:$scope.formData}
            }).then(function (response) {
                //    .draw
                dataTable.row.add(['nikhil', 'JBL-9881771', 'KUKA 12V SML', 'LISBON, SPAIN', '1990', '90%', '09/12/2016',
                '<img src="assets/img/data_table_BlueStatus_16.fw.png"><img src="assets/img/data_table_ClearStatus_16.fw.png"><img src="assets/img/data_table_RedStatus_16.fw.png">',
                'Mike T','20%','3122','$99,999','$29,999,999.00']).draw();;
            }).catch();

        }

        $scope.init();
    });
