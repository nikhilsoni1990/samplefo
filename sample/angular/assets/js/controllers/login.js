'use strict';

/* Controllers */

angular.module('app')
    .controller('LoginCtrl', function ($scope, $state, $stateParams, $http) {
        $scope.onLoginSubmit = function () {
            $http({
                "url": 'http://ec2-52-53-208-151.us-west-1.compute.amazonaws.com:8000/sessions/login',
                "method": "POST",
                "headers": {
                "Content-Type": "application/json"
                },
                "cache": true,
                "data": {payload:$scope.formData}
            }).then(function (response) {
                localStorage.setItem('jwt', response.data.data.jwt);
                localStorage.setItem('sessionId', response.data.data.sessionId);
                $state.go('app.dashboard', response);
               }).catch();

        }

    });
