/* ============================================================
 * File: config.js
 * Configure routing
 * ============================================================ */

angular.module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider',

        function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
            $urlRouterProvider
                .otherwise('/login');

            $stateProvider
                .state('app', {
                   // abstract: true,
                    url: "/app",
                    templateUrl: "tpl/app.html",
                })
                .state('login', {
                    url: "/login",
                    templateUrl: "tpl/login.html",
                    controller: 'LoginCtrl',
                })
                .state('app.tenants', {
                    url: "/tenants",
                    templateUrl: "tpl/tenants.html",
                    controller: 'TenantsCTRL',
                    resolve: {
                                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                    'select',
                                    'tagsInput',
                                    'dataTables',

                                    ],  {
                                            insertBefore: '#lazyload_placeholder'
                                        })
                                        .then(function() {
                                            return $ocLazyLoad.load([
                                                'assets/js/controllers/tenants.js'
                                            ]);
                                        });
                                }]
                            }
                })
                .state('app.dashboard', {
                     url: "/home",
                     templateUrl: "tpl/dashboard.html"
                 })

        }
    ]);

//angular.module("app").run(function ($rootScope, $state, AuthService) {
//    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
//        alert('athunticate');
//    });
//});