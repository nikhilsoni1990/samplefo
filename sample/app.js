var express = require('express');
var app = express();

require('./settings/express').configureApp(app);
require('./routes')(app);
module.exports = app;
